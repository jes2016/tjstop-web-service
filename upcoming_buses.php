<?php
require_once "db-connect.php";
$connect = connect_db();
$id_halte = mysqli_real_escape_string($connect, $_GET["id_halte"]);

$query = "SELECT b.id_bus, b.nama, b.lat, b.lon FROM Track t, Bus b where t.id_halte_tujuan=".$id_halte." AND t.id_bus = b.id_bus order by 1";

$result = mysqli_query($connect, $query);
$busList = array();
$hasil;


if($result)
{

  while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        $bus = array("id_bus" => $row["id_bus"], "nama" => $row["nama"],
                      "lat" => $row["lat"],"lon" => $row["lon"]);
    //ubah ke bentuk array
    array_push($busList, $bus);
  }
  $hasil = array("error" => "false", "busList" => $busList);
}
else{
  $hasil = array("error" => true);
  //$hasil = array("error" => "true", "chat_box" => $chatboxes);
}


header('Content-Type: application/json');
echo json_encode($hasil);
 ?>
