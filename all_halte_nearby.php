<?php
require_once "db-connect.php";
$connect = connect_db();
$latFrom = mysqli_real_escape_string($connect, $_GET["lat"]);
$lonFrom = mysqli_real_escape_string($connect, $_GET["lon"]);
function distance($lat1, $lon1, $lat2, $lon2){
//   $latitudeFrom = $lat;
//   $longitudeFrom = $lon;
//
//   $latitudeTo = $lat1;
//   $longitudeTo = $lon1;
//
// //Calculate distance from latitude and longitude
//   $theta = $longitudeFrom - $longitudeTo;
//   $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
//   $dist = acos($dist);
//   $dist = rad2deg($dist);
//   $miles = $dist * 60 * 1.1515;
//
//   $distance = ($miles * 1.60934);
//  return $distance;

  $pi80 = M_PI / 180;
    $lat1 *= $pi80;
    $lon1 *= $pi80;
    $lat2 *= $pi80;
    $lon2 *= $pi80;

    $r = 6372.797; // mean radius of Earth in km
    $dlat = $lat2 - $lat1;
    $dlon = $lon2 - $lon1;
    $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $km = $r * $c;

    //echo '<br/>'.$km;
    return $km;
}

$Halte;
$query = "SELECT * FROM Halte";
// $result = mysqli_query($connect, $sql);
// $hasil = 0 ;
// if (mysqli_num_rows($result) > 0) {
//     // output data of each row
//     while($row = mysqli_fetch_assoc($result)) {
//         array_push($halteList, (Integer) $row["waktu"]);
//         $Halte = array("id_halte" => $row["id_halte"], "nama" =>);
//         // $halteList.add();
//     }
// } else {
//     echo "0 results";
// }

$result = mysqli_query($connect, $query);
$halteList = array();
$hasil;


if($result)
{


  while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $distance = distance($latFrom, $lonFrom, $row["lat"], $row["lon"]);
        $halte = array("id_halte" => $row["id_halte"], "nama" => $row["nama"],
                        "alamat" => $row["alamat"], "lat" => $row["lat"],"lon" => $row["lon"],
                        "descr" => $row["descr"], "distance" => $distance);
    //ubah ke bentuk array


    array_push($halteList, $halte);
  }
  $hasil = array("error" => "false", "halteList" => $halteList);
}
else{
  $hasil = array("error" => true);
  //$hasil = array("error" => "true", "chat_box" => $chatboxes);
}


header('Content-Type: application/json');
echo json_encode($hasil);


?>
